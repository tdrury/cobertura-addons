package net.sourceforge.cobertura.ws;

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

import net.sourceforge.cobertura.coveragedata.ProjectData;
import net.sourceforge.cobertura.coveragedata.TouchCollector;

import org.apache.commons.codec.binary.Base64;

@WebService(name = "Fetch", targetNamespace = "net:sourceforge:cobertura")
public class Fetch {

	@WebMethod
    @WebResult(name = "ProjectData", targetNamespace = "net:sourceforge:cobertura")
	public WrappedProjectData fetch() {
		StringBuilder status = new StringBuilder();
		WrappedProjectData wrappedProjectData = new WrappedProjectData();
		
		ProjectData projectData = new ProjectData();
		TouchCollector.applyTouchesOnProjectData(projectData);
		wrappedProjectData.setCoverage(projectData.getLineCoverageRate());
		wrappedProjectData.setClassCount(projectData.getNumberOfClasses());
		wrappedProjectData.setSourceFileCount(projectData.getNumberOfSourceFiles());
		
		String serializedProjectData = null;
		try {
			serializedProjectData = serializeProjectData(projectData);
		} catch (Exception ex) {
			status.append("{could not serialize ProjectData: ").append(ex.getMessage()).append("}");
		}
		wrappedProjectData.setSerializedProjectData(serializedProjectData);
		
		wrappedProjectData.setStatus(status.toString());
		return wrappedProjectData;
	}
	
	protected String serializeProjectData(ProjectData projectData) throws IOException {
		ObjectOutputStream oos = null;      
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(projectData);
		} finally {
			if (oos != null) try { oos.close(); } catch (IOException ex) { }
			if (baos != null) try { baos.close(); } catch (IOException ex) { }
		}
		if (baos != null) {
			String encoded = Base64.encodeBase64String(baos.toByteArray());
			return encoded;
		} else {
			return null;
		}
	}
}
