package net.sourceforge.cobertura.ws;

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import java.util.Date;

public class WrappedProjectData  {

	protected String serializedProjectData;
	protected Date collectedDate;
	protected Integer sourceFileCount;
	protected Integer classCount;
	protected Double coverage;
	protected String status = "?";

	public void setSerializedProjectData(String serializedProjectData) { this.serializedProjectData = serializedProjectData; }
	public String getSerializedProjectData() { return serializedProjectData; }
	
	public void setCollectedDate(Date collectedDate) { this.collectedDate = collectedDate; }
	public Date getCollectedDate() { return collectedDate; }
	
	public Integer getSourceFileCount() { return sourceFileCount; }
	public void setSourceFileCount(Integer sourceFileCount) { this.sourceFileCount = sourceFileCount; }
	
	public Integer getClassCount() { return classCount; }
	public void setClassCount(Integer classCount) { this.classCount = classCount; }

	public Double getCoverage() { return coverage; }
	public void setCoverage(Double coverage) { this.coverage = coverage; }
	
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	public WrappedProjectData() {
		collectedDate = new Date();
	}

}
