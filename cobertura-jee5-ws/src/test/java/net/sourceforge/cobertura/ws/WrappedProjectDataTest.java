package net.sourceforge.cobertura.ws;

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import static org.testng.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

import net.sourceforge.cobertura.coveragedata.CoverageDataFileHandler;
import net.sourceforge.cobertura.coveragedata.ProjectData;

import org.apache.commons.codec.binary.Base64;
import org.testng.annotations.Test;

public class WrappedProjectDataTest {

	public static final String DATAFILE = "src/test/resources/cobertura.ser";
	@Test
	public void testProjectDataSerialization() {
		
		System.out.println("testProjectDataSerialization: starting...");
		ProjectData projectData = CoverageDataFileHandler.loadCoverageData(new File(DATAFILE));
		assertNotNull(projectData);
		
		int numberOfClasses = projectData.getNumberOfClasses();
		int numberOfCoveredLines = projectData.getNumberOfCoveredLines();
		
		Fetch fetch = new Fetch();
		
		String serializedProjectData = null;
		try {
			serializedProjectData = fetch.serializeProjectData(projectData);
		} catch (IOException ex) {
			fail("could not serialize ProjectData", ex);
		}
		assertNotNull(serializedProjectData);
		System.out.println("testProjectDataSerialization: serialized project data size ["+serializedProjectData.length()+"]");
		//System.out.println("testProjectDataSerialization: serialized project data ["+serializedProjectData+"]");
		
		ProjectData pd2 = null;
		try {
			pd2 = deserializeProjectData(serializedProjectData);
		} catch (Exception ex) {
			fail("could not deserialize project data", ex);
		}
		assertNotNull(pd2);
		
		assertEquals(pd2.getNumberOfClasses(), numberOfClasses);
		assertEquals(pd2.getNumberOfCoveredLines(), numberOfCoveredLines);
		
		System.out.println("testProjectDataSerialization: done.");
		
	}
	
	protected ProjectData deserializeProjectData(String serializedProjectData) throws IOException, ClassNotFoundException
	{
		byte[] ba = Base64.decodeBase64(serializedProjectData);
		ByteArrayInputStream bais = new ByteArrayInputStream(ba);
		
		ObjectInputStream ois = null;
		try	{
			ois = new ObjectInputStream(bais);
			ProjectData projectData = (ProjectData)ois.readObject();
			return projectData;
		} finally {
			if (ois != null) try { ois.close(); } catch (IOException ex) { }
			if (bais != null) try { bais.close(); } catch (IOException ex) { }
		}
	}
}
