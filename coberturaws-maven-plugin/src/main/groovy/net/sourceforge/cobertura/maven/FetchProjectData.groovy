package net.sourceforge.cobertura.maven

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import net.sourceforge.cobertura.coveragedata.ProjectData
import wslite.soap.*

import org.apache.maven.project.MavenProject
import org.apache.maven.plugin.MojoFailureException
import org.codehaus.gmaven.mojo.GroovyMojo
import org.jfrog.maven.annomojo.annotations.MojoGoal
import org.jfrog.maven.annomojo.annotations.MojoPhase
import org.jfrog.maven.annomojo.annotations.MojoParameter

@MojoGoal('fetch')
@MojoPhase('integration-test')
class FetchProjectData extends GroovyMojo {
	
	/**
	 * URL of the Cobertura web service WSDL
	 */
	@MojoParameter(required=true)
	public String wsdlUrl

	/**
	 * name of cobertura output file (default: cobertura/cobertura.ser)
	 */
	@MojoParameter(defaultValue='cobertura/cobertura.ser')
	public String outputFile = "cobertura/cobertura.ser"
	
	/**
	 * maven project
	 */
	@MojoParameter(expression='${project}', required=true, readonly=true)
	public MavenProject mavenProject
	
	
	void execute() throws MojoFailureException
	{
		log.info "creating client from WSDL ["+wsdlUrl+"]"
		def client = new SOAPClient(wsdlUrl)

		log.info "calling Cobertura web service to fetch project data..."
		def response = client.send(SOAPAction:"") {
			body {
				fetch('xmlns':'net:sourceforge:cobertura')
			}
		}
		log.info "fetch status is ["+response.fetchResponse.ProjectData.status+"]"
		log.info "collection date ["+response.fetchResponse.ProjectData.collectedDate+"]"
		log.info "source file count ["+response.fetchResponse.ProjectData.sourceFileCount+"]"
		log.info "class count ["+response.fetchResponse.ProjectData.classCount+"]"
		log.info "coverage ["+response.fetchResponse.ProjectData.coverage+"]"
		
		// declare our ProjectData
		def pd = new ProjectData()

		try {
			// bais is automatically closed after the closure
			def bais = new ByteArrayInputStream(response.fetchResponse.ProjectData.serializedProjectData.text().decodeBase64())
			bais.withObjectInputStream(getClass().classLoader) { ois->
				pd = ois.readObject()
			}
		} catch (Exception ex) {
			throw new MojoFailureException("failed to decode/deserialize the Cobertura ProjectData", ex)
		}
		
		
		def targetFile = new File(mavenProject.getBuild().getDirectory(), outputFile)
		if (targetFile.exists()) {
			if (!targetFile.delete()) {
				throw new MojoFailureException("could not delete old data file ["+targetFile+"]")
			}
		}
		
		def targetDirectory = targetFile.getParentFile()
		if (!targetDirectory.exists()) {
			log.info "creating working directory ["+targetDirectory+"]"
			if (!targetDirectory.mkdirs()) throw new MojoFailureException("could not create target directory ["+targetDirectory+"]")
		}
		
		// save the cobertura project data to a file		
		log.info "saving Cobertura project data to ["+targetFile+"]"		
		targetFile.withObjectOutputStream { oos->
			oos.writeObject(pd)
		}		
	}
}