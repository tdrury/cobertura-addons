package net.sourceforge.cobertura.maven

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import net.sourceforge.cobertura.coveragedata.ProjectData
import net.sourceforge.cobertura.coveragedata.CoverageDataFileHandler
import org.codehaus.plexus.util.FileUtils

import org.apache.maven.model.FileSet
import org.apache.maven.project.MavenProject
import org.apache.maven.plugin.MojoFailureException
import org.codehaus.gmaven.mojo.GroovyMojo
import org.jfrog.maven.annomojo.annotations.MojoGoal
import org.jfrog.maven.annomojo.annotations.MojoPhase
import org.jfrog.maven.annomojo.annotations.MojoParameter

@MojoGoal('merge')
@MojoPhase('integration-test')
class MergeProjectData extends GroovyMojo {
	

	/**
	 * set of cobertura files to merge
	 */
	@MojoParameter(required=true)
	public FileSet coberturaFiles
	
	/**
	 * name of cobertura merge output file (default: cobertura/cobertura.ser)
	 */
	@MojoParameter(defaultValue='cobertura/cobertura.ser')
	public String outputFile = "cobertura/cobertura.ser"

	/**
	 * maven project
	 */
	@MojoParameter(expression='${project}', required=true, readonly=true)
	public MavenProject mavenProject
	
	
	void execute() throws MojoFailureException
	{
		def directory = new File(coberturaFiles.getDirectory())
        def includes =coberturaFiles.getIncludes().join(",") 
        def excludes = coberturaFiles.getExcludes().join(",")
		log.info "fileset directory ["+directory+"]"
		log.info "fileset includes ["+includes+"]"
		log.info "fileset excludes ["+excludes+"]"
		log.info "fileset resolved list of files:"
        def coberturaFileList = FileUtils.getFiles(directory, includes, excludes)
		coberturaFileList.each {
			log.info "   ["+it+"]"
		}
		
		def targetFile = new File(mavenProject.getBuild().getDirectory(), outputFile)
		def targetDirectory = targetFile.getParentFile()
		if (!targetDirectory.exists()) {
			log.info "creating working directory ["+targetDirectory+"]"
			if (!targetDirectory.mkdirs()) throw new MojoFailureException("could not create target directory ["+targetDirectory+"]")
		}

		// save the cobertura project data to a file		
		log.info "merging Cobertura project data to ["+targetFile+"]"
		
		def mergedProjectData = new ProjectData()
		
		coberturaFileList.each { f->
			def pd = CoverageDataFileHandler.loadCoverageData(f)
			log.info "   ProjectData ["+f+"]"
			log.info "      "+coverageSummary(pd)
			mergedProjectData.merge(pd)
			log.info "      merged: "+coverageSummary(mergedProjectData)
		}
		
		log.info "final merged ProjectData: "+coverageSummary(mergedProjectData)
		
		if (targetFile.exists()) {
			if (!targetFile.delete()) throw new MojoFailureException("could not delete old coverage data file ["+targetFile+"]")
		}
		targetFile.withObjectOutputStream { oos->
			oos.writeObject(mergedProjectData)
		}
				
	}
	
	String coverageSummary(ProjectData pd) {
		def sb = new StringBuilder("{")
		sb.append("#sources:[").append(pd.getNumberOfSourceFiles()).append("]")
		  .append("#classes:[").append(pd.getNumberOfClasses()).append("]")
		  .append("l/c/rate:[").append(pd.getNumberOfValidLines()).append("/").append(pd.getNumberOfCoveredLines())
		  	.append("/").append(pd.getLineCoverageRate()).append("]")
		  .append("b/c/rate:[").append(pd.getNumberOfValidBranches()).append("/").append(pd.getNumberOfCoveredBranches())
		  	.append("/").append(pd.getBranchCoverageRate()).append("]")
		  .append("}");
		return sb.toString()
	}
	
}