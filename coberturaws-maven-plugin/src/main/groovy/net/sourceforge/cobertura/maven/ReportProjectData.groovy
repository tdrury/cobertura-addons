package net.sourceforge.cobertura.maven

/* Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License. */

import net.sourceforge.cobertura.coveragedata.ProjectData
import net.sourceforge.cobertura.coveragedata.CoverageDataFileHandler
import net.sourceforge.cobertura.util.FileFinder
import net.sourceforge.cobertura.reporting.html.HTMLReport
import net.sourceforge.cobertura.reporting.xml.SummaryXMLReport
import net.sourceforge.cobertura.reporting.xml.XMLReport
import net.sourceforge.cobertura.reporting.ComplexityCalculator
import net.sourceforge.cobertura.util.Header

import org.codehaus.plexus.util.FileUtils

import org.apache.maven.model.FileSet
import org.apache.maven.project.MavenProject
import org.apache.maven.plugin.MojoFailureException
import org.codehaus.gmaven.mojo.GroovyMojo
import org.jfrog.maven.annomojo.annotations.MojoGoal
import org.jfrog.maven.annomojo.annotations.MojoPhase
import org.jfrog.maven.annomojo.annotations.MojoParameter

@MojoGoal('report')
@MojoPhase('verify')
class ReportProjectData extends GroovyMojo {
	
	/**
	 * report format (xml|html). default is html.
	 */
	@MojoParameter(defaultValue='html')
	public String format = "html"

	/**
	* report encoding. default is UTF-8.
	*/
    @MojoParameter(defaultValue='UTF-8')
    public String encoding = "UTF-8"

	/**
	 * name of cobertura data file (default: cobertura/cobertura.ser)
	 */
	@MojoParameter(defaultValue='cobertura/cobertura.ser')
	public String dataFile = "cobertura/cobertura.ser"

	/**
	* report output directory (default: cobertura-report)
	*/
    @MojoParameter(defaultValue='cobertura-report')
    public String outputDirectory = "cobertura-report"

	/**
	* source files
	*/
    @MojoParameter
    public FileSet sourceDirectories

	/**
	 * maven project
	 */
	@MojoParameter(expression='${project}', required=true, readonly=true)
	public MavenProject mavenProject
	
	
	void execute() throws MojoFailureException
	{
		File coberturaDataFile = new File(mavenProject.getBuild().getDirectory(), dataFile)
		if (!coberturaDataFile.exists()) {
			log.info "cobertura data file ("+dataFile+") not found - skipping report generation"
			return
		}
		
		def projectData = CoverageDataFileHandler.loadCoverageData(coberturaDataFile)
		if (projectData == null) {
			throw new MojoFailureException("cobertura coverage data is null")
		}
		
		def outputDirectoryFile = new File(mavenProject.getBuild().getDirectory(), outputDirectory)
		if (!outputDirectoryFile.exists()) {
			log.info "creating output directory ["+outputDirectoryFile+"]"
			if (!outputDirectoryFile.mkdirs()) throw new MojoFailureException("could not create output directory ["+outputDirectoryFile+"]")
		}

		def sourceDirectoryList		
		if (sourceDirectories != null) {
			def directory = new File(sourceDirectories.getDirectory())
			def includes = sourceDirectories.getIncludes().join(",")
			def excludes = sourceDirectories.getExcludes().join(",")
			log.info "sourceDirectories: fileset directory ["+directory+"]"
			log.info "sourceDirectories: fileset includes ["+includes+"]"
			log.info "sourceDirectories: fileset excludes ["+excludes+"]"
			sourceDirectoryList = FileUtils.getDirectoryNames(directory, includes, excludes, true)
		} else {
			sourceDirectoryList = mavenProject.getCompileSourceRoots()
		}

		def finder = new FileFinder()
		log.info "source directories:"
		sourceDirectoryList.each {
			log.info "   ["+it+"]"
			finder.addSourceDirectory(it)
		}

		
		def complexity = new ComplexityCalculator(finder)
		switch (format) {
			case 'html':
				log.info "generating cobertura HTML report..."
				def report = new HTMLReport(projectData, outputDirectoryFile, finder, complexity, encoding)
				break
			case 'xml':
				log.info "generating cobertura XML report..."
				def report = new XMLReport(projectData, outputDirectoryFile, finder, complexity)
				break
			default:
				log.info "report format ["+format+"] not recognized; generating cobertura XML summary report..."
				def report = new SummaryXMLReport(projectData, outputDirectoryFile, finder, complexity)
		}
		
	}
	
}